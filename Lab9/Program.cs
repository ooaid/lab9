﻿using Lab9.DataTree;
namespace Lab9
{
    class Program
    {
        public static void Main()
        {
            TreeBranch<object> tree = new TreeBranch<object>();
            tree.Add(new TreeLeaf<object>(1));
            tree.Add(new TreeLeaf<object>(2));
            tree.Add(new TreeLeaf<object>(3));

            TreeBranch<object> treeBranch1 = new TreeBranch<object>();
            treeBranch1.Add(new TreeLeaf<object>(4));
            treeBranch1.Add(new TreeLeaf<object>(5));
            treeBranch1.Add(new TreeLeaf<object>(6));

            TreeBranch<object> treeBranch2 = new TreeBranch<object>();
            treeBranch2.Add(new TreeLeaf<object>(7));
            treeBranch2.Add(new TreeLeaf<object>(8));
            treeBranch2.Add(new TreeLeaf<object>(9));

            TreeBranch<object> treeBranch2_1 = new TreeBranch<object>();
            treeBranch2_1.Add(new TreeLeaf<object>(10));
            treeBranch2_1.Add(new TreeLeaf<object>(11));
            treeBranch2_1.Add(new TreeLeaf<object>(12));

            treeBranch2.Add(treeBranch2_1);
            treeBranch1.Add(treeBranch2);
            tree.Add(treeBranch1);
            Console.WriteLine(tree.AsString());
            Console.WriteLine(tree.AsPrettyString());



        }
    }
}