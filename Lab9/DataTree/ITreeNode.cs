﻿namespace Lab9.DataTree
{
    public interface ITreeNode<T>
    {
        public string AsString();
        public string AsPrettyString(int depth=0);
    }
}
