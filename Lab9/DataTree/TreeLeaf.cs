﻿namespace Lab9.DataTree
{
    public class TreeLeaf<T> : ITreeNode<T>
    {
        public T Value { get; private set; }
        public TreeLeaf(T value)
        {
            Value = value;
        }

        public string AsString()
        {
            return $"\"{Value}\"";
        }

        public string AsPrettyString(int depth)
        {
            return this.AsString();
        }
    }
}
