﻿namespace Lab9.DataTree
{
    public class TreeBranch<T> : ITreeNode<T>
    {
        public List<ITreeNode<T>> Children { get; private set; }
        public TreeBranch()
        {
            Children = new List<ITreeNode<T>>();
        }
        public void Add(ITreeNode<T> node)
        {
            Children.Add(node);
        }
        public void Remove(ITreeNode<T> node)
        {
            Children?.Remove(node);
        }
        public string AsString()
        {
            return $"[{string.Join(", ", Children.Select(x => x.AsString()))}]";
        }
        public string AsPrettyString(int depth=0)
        {
            return "[" +string.Join(", " + System.Environment.NewLine, Children
                .Take(1)
                .Select(x => x.AsPrettyString())
                .Concat(
                    Children
                    .Skip(1)
                    .Select(x => new string(' ', depth+1) + x.AsPrettyString(depth + 1))))
                +"]";
        }
    }
}
