using NUnit.Framework;
using Lab9.DataTree;

namespace Lab9_Tests
{
    public class Tests
    {
        [Test]
        public void TestDisplayForEmptyComposite()
        {
            ITreeNode<int> branch = new TreeBranch<int>();
            Assert.AreEqual("[]", branch.AsString());
        }

        [Test]
        public void TestDisplayForOneLayerComposite()
        {
            TreeBranch<int> branch = new TreeBranch<int>();
            branch.Add(new TreeLeaf<int>(1));
            branch.Add(new TreeLeaf<int>(2));
            branch.Add(new TreeLeaf<int>(3));
            Assert.AreEqual("[\"1\", \"2\", \"3\"]", branch.AsString());
        }
        [Test]
        public void TestDisplayForTwoLayerComposite()
        {
            TreeBranch<int> branch = new TreeBranch<int>();
            TreeBranch<int> branch1 = new TreeBranch<int>();
            TreeBranch<int> branch2 = new TreeBranch<int>();
            branch1.Add(new TreeLeaf<int>(1));
            branch1.Add(new TreeLeaf<int>(2));
            branch1.Add(new TreeLeaf<int>(3));
            branch2.Add(new TreeLeaf<int>(4));
            branch2.Add(new TreeLeaf<int>(5));
            branch2.Add(new TreeLeaf<int>(6));
            branch.Add(branch1);
            branch.Add(branch2);
            Assert.AreEqual("[[\"1\", \"2\", \"3\"], [\"4\", \"5\", \"6\"]]", branch.AsString());
        }
        [Test]
        public void TestDisplayForThreeLayerComposite()
        {
            TreeBranch<int> branch = new TreeBranch<int>();
            TreeBranch<int> branch1 = new TreeBranch<int>();
            TreeBranch<int> branch11 = new TreeBranch<int>();
            TreeBranch<int> branch12 = new TreeBranch<int>();
            TreeBranch<int> branch2 = new TreeBranch<int>();
            TreeBranch<int> branch21 = new TreeBranch<int>();
            TreeBranch<int> branch22 = new TreeBranch<int>();

            branch11.Add(new TreeLeaf<int>(1));
            branch11.Add(new TreeLeaf<int>(2));
            branch12.Add(new TreeLeaf<int>(3));
            branch12.Add(new TreeLeaf<int>(4));
            branch21.Add(new TreeLeaf<int>(5));
            branch21.Add(new TreeLeaf<int>(6));
            branch22.Add(new TreeLeaf<int>(7));
            branch22.Add(new TreeLeaf<int>(8));
            branch.Add(branch1);
            branch1.Add(branch11);
            branch1.Add(branch12);
            branch.Add(branch2);
            branch2.Add(branch21);
            branch2.Add(branch22);
            Assert.AreEqual("[[[\"1\", \"2\"], [\"3\", \"4\"]], [[\"5\", \"6\"], [\"7\", \"8\"]]]", branch.AsString());
        }
    }
}